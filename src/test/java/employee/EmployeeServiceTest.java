package employee;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EmployeeServiceTest {

    private final EmployeeRepository employeeRepository = new EmployeeRepository();
    private final EmployeeService employeeService = new EmployeeService(employeeRepository);
    private final Employee employee = new Employee(120,"Marek", "Marecki", 250);


    @Test
    void should_add_new_Employee() {
        //given
        int sizeBefore = employeeRepository.employees.size();
        int sizeAfter = sizeBefore + 1;

        //when
        employeeService.addEmployee(employee);

        //then
        assertEquals(sizeAfter, sizeBefore + 1);
        assertTrue(employeeRepository.getAll().contains(employee));

    }

    @Test
    void should_findById() {
        //given
        employeeRepository.addEmployee(employee);

        //when
        Employee employee = employeeService.findById(120);

        //then
        assertEquals(120, employee.getId());
        assertEquals("Marek", employee.getFirstName());
        assertEquals("Marecki", employee.getLastName());
        assertEquals(250, employee.getSalary());

    }

    @Test
    void should_return_null_when_not_found_by_id() {
        //when
        Employee employee = employeeService.findById(120);

        //then
        assertNull(employee);
    }


    @Test
    void should_return_lis_of_all_employees() {

        //given, when
        int sizeOfSet = employeeRepository.employees.size();

        //then
        assertEquals(sizeOfSet, employeeService.getAll().size());

    }
}