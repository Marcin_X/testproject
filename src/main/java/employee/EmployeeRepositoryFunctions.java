package employee;

import java.util.List;

interface EmployeeRepositoryFunctions {

    void addEmployee(Employee employee);

    Employee getEmployeeById(int id);

    List<Employee> getAll();

}
