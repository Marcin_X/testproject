package employee;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public void addEmployee(Employee employee) {
        employeeRepository.addEmployee(employee);
    }

    public Employee findById(int id){
        return employeeRepository.getEmployeeById(id);
    }

    public List<Employee> getAll() {
        return employeeRepository.getAll();
    }
}
