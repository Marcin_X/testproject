package employee;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeRepository implements EmployeeRepositoryFunctions {
    Set<Employee> employees = new HashSet<>();


    {
        employees.add(new Employee(100, "Marcin", "Klasicki", 200));
        employees.add(new Employee(101, "Rafix", "Nowak", 250));
        employees.add(new Employee(102, "Seba", "Typowy", 100));
        employees.add(new Employee(103, "Stefanek", "Stefański", 150));

    }

    @Override
    public void addEmployee(Employee employee) {
        if (employees.contains(employee)) {
            System.out.println("Can't add this employee already exists");
        }

        employees.add(employee);
        System.out.println("Added new employee");
    }

    @Override
    public Employee getEmployeeById(int id) {
        for (Employee e : employees) {
            if(e.getId() == id) {
                System.out.println("Found employee with id  " + id);
                return e;

            }
        }
        System.out.println("There is no employee with id " + id);
        return null;
    }

    @Override
    public List<Employee> getAll() {
        System.out.println("There is " + employees.size() + " employees on the list");
       return new ArrayList<>(employees);
    }
}
